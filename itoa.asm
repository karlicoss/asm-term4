extern printf

section .data
    format db "%d",10,0
    buffer db "            "

section .text
    global main

itoa: ; (bufffer, int) -> itoa(int) in buffer
    push edi
    push esi
    mov edi, [esp + 12] ; buffer 
    mov ecx, [esp + 16] ; current value
    mov ebx, 0 ; digits count
   
    mov esi, 10 ; base 

    loop_digits:
        inc ebx    
        
        xor edx, edx
        mov eax, ecx
        div esi
        
        push edx ; ecx % 10 into stack
        mov ecx, eax ; ecx = ecx / 10
          
        cmp ecx, 0  
    jnz loop_digits ; do while ecx > 0
    
    mov ecx, 0 ; loop iterator
    
    loop_string:
        pop eax
        add al, '0'
        mov [edi + ecx], al
        inc ecx
        cmp ecx, ebx
    jl loop_string ; while ebx < ecx    
   
    mov eax, 0 
    mov [edi + ebx], al; ; null terminator 

    mov eax, edi ; return value

    pop esi
    pop edi
    ret 8

main:
    push 1243
    push buffer
    call itoa

    push buffer
    call printf
    add esp, 4    

    mov eax, 0
    ret    

